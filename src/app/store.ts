import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import charactersReducer from "../features/characters/charactersSlice";
import seriesSlice from "../features/series/seriesSlice";

export const store = configureStore({
  reducer: {
    characters: charactersReducer,
    series: seriesSlice,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

import React from "react";
import { render } from "../common/test-utils";
import { Header } from "./Header";

test("renders correctly", () => {
  const { container } = render(<Header />);

  expect(container).toMatchSnapshot();
});

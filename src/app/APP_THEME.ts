import tailwindTheme from "@theme-ui/preset-tailwind";

export type AppTheme = typeof tailwindTheme;

const APP_THEME: AppTheme = {
  ...tailwindTheme,
  colors: {
    ...tailwindTheme.colors,
    primary: tailwindTheme.colors.dark,
  },
};

export default APP_THEME;

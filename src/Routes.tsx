import React, { FC } from "react";
import { Route, Switch } from "react-router-dom";
import CharacterDetail from "./features/characters/CharacterDetail";
import CharactersIndex from "./features/characters/CharactersIndex";

const Routes: FC = () => {
  return (
    <Switch>
      <Route path="/" exact component={CharactersIndex} />
      <Route path="/characters/:id" component={CharacterDetail} />
    </Switch>
  );
};

export default Routes;

import React from "react";
import BaseLayout from "./app/BaseLayout";
import { Header } from "./app/Header";
import Routes from "./Routes";

function App() {
  return (
    <BaseLayout header={<Header />}>
      <Routes />
    </BaseLayout>
  );
}

export default App;

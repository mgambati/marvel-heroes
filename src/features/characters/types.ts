import { ImageResource } from "../../common/types";

export interface Character {
  id: number;
  name: string;
  description: string;
  thumbnail: ImageResource;
}

export interface EditableCharacterFields {
  name: string;
  description: string;
}

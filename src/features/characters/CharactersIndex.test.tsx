import React from "react";
import {
  fireEvent,
  getByTestId,
  getByText,
  render,
  waitFor,
} from "../../common/test-utils";
import * as charactersApi from "./charactersApi";
import CharactersIndex from "./CharactersIndex";
import { CHARACTERS_RESPONSE_PAYLOAD } from "./__fixtures__/charactersRequests";

const emptyPayload = {
  offset: 0,
  limit: 2,
  total: 1493,
  count: 0,
  results: [],
};

test("should render loading indicator", async () => {
  const { container } = render(<CharactersIndex />);
  const input = getByTestId(container, "search-input");
  fireEvent.change(input, { target: { value: "loki" } });

  await waitFor(() => {
    expect(
      getByTestId(container, "loading-characters-spinner")
    ).toBeInTheDocument();
  });
});

test("should render cards", async () => {
  jest
    .spyOn(charactersApi, "fetchCharacters")
    .mockResolvedValueOnce(emptyPayload);

  const { container } = render(<CharactersIndex />);

  jest
    .spyOn(charactersApi, "fetchCharacters")
    .mockResolvedValueOnce(CHARACTERS_RESPONSE_PAYLOAD);

  // Fake search
  const input = getByTestId(container, "search-input");
  fireEvent.change(input, { target: { value: "3-D Man" } });

  await waitFor(() => {
    expect(getByText(container, "3-D Man")).toBeInTheDocument();
  });
});

import React, { FC } from "react";
import { Flex, Heading } from "theme-ui";
import CharacterSearch from "./CharacterSearch";
import CharactersList from "./CharactersList";

export interface CharactersIndexProps {}

const CharactersIndex: FC<CharactersIndexProps> = () => {
  return (
    <Flex sx={{ flexDirection: "column" }}>
      <Heading>Personagens Marvel</Heading>
      <CharacterSearch />
      <CharactersList />
    </Flex>
  );
};

export default CharactersIndex;

import React from "react";
import { fireEvent, render, waitFor } from "../../common/test-utils";
import * as charactersApi from "./charactersApi";
import CharactersList from "./CharactersList";
import * as charactersSlice from "./charactersSlice";
import { CHARACTERS_RESPONSE_PAYLOAD } from "./__fixtures__/charactersRequests";

beforeEach(() => {
  jest
    .spyOn(charactersSlice, "selectAllCharactersIds")
    .mockReturnValue(
      CHARACTERS_RESPONSE_PAYLOAD.results.map((item) => item.id)
    );

  jest
    .spyOn(charactersApi, "fetchCharacters")
    .mockResolvedValueOnce(CHARACTERS_RESPONSE_PAYLOAD)
    .mockResolvedValueOnce(CHARACTERS_RESPONSE_PAYLOAD);
});

beforeEach(() => {
  jest.clearAllMocks();
});

test("renders a list of CharactersCard", async () => {
  const { findAllByTestId } = render(<CharactersList />);

  expect(await findAllByTestId("character-card")).toHaveLength(2);
});

test("loads more characters", async () => {
  const { getByTestId } = render(<CharactersList />);

  const loadMoreButton = getByTestId("load-more-btn");
  fireEvent.click(loadMoreButton);

  await waitFor(() => {
    expect(charactersApi.fetchCharacters).toHaveBeenCalledTimes(1);
  });
});

import {
  createAsyncThunk,
  createEntityAdapter,
  createSelector,
  createSlice,
  Dictionary,
} from "@reduxjs/toolkit";
import { RootState } from "../../app/store";
import { ResourceStatus } from "../../common/types";
import { fetchCharacters, fetchSingleCharacter } from "./charactersApi";
import { Character } from "./types";

//=================================================
// Interfaces
//=================================================

export interface CharactersState {
  status: Dictionary<ResourceStatus>;
  offset: number;
  total: number;
  query?: string;
}

//=================================================
// Thunks
//=================================================

export interface LoadCharactersOptions {
  query?: string;
  offset: number;
}

export const loadCharacters = createAsyncThunk(
  "characters/loadCharacters",
  async (options: LoadCharactersOptions) => {
    return await fetchCharacters(options.offset, options.query);
  }
);

export const loadSingleCharacter = createAsyncThunk(
  "characters/loadSingleCharacter",
  async (id: string | number) => {
    return await fetchSingleCharacter(id);
  }
);

//=================================================
// Slice
//=================================================

const charactersAdapter = createEntityAdapter<Character>();

const charactersSlice = createSlice({
  name: "characters",
  initialState: charactersAdapter.getInitialState<CharactersState>({
    query: undefined,
    offset: 0,
    total: 0,
    status: {
      all: "idle",
    },
  }),
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(loadCharacters.pending, (state) => {
      state.status["all"] = "pending";
    });

    builder.addCase(loadCharacters.fulfilled, (state, action) => {
      const hasQueryChanged = state.query !== action.meta.arg.query;
      const hasNoOffset = action.payload.offset === 0;

      hasQueryChanged || hasNoOffset
        ? charactersAdapter.setAll(state, action.payload.results)
        : charactersAdapter.addMany(state, action.payload.results);

      state.status["all"] = "idle";
      state.query = action.meta.arg.query;
      state.offset = action.payload.offset;
      state.total = action.payload.total;
    });

    builder.addCase(loadCharacters.rejected, (state) => {
      state.status["all"] = "error";
    });

    builder.addCase(loadSingleCharacter.pending, (state, action) => {
      const characterId = action.meta.arg;
      state.status[characterId] = "pending";
    });

    builder.addCase(loadSingleCharacter.fulfilled, (state, action) => {
      const characterId = action.meta.arg;
      charactersAdapter.upsertOne(state, action.payload);
      state.status[characterId] = "idle";
    });
    builder.addCase(loadSingleCharacter.rejected, (state, action) => {
      const characterId = action.meta.arg;
      state.status[characterId] = "error";
    });
  },
});

//=================================================
// Selectors
//=================================================

const charactersSelectors = charactersAdapter.getSelectors();

const selectCharacterState = (state: RootState) => state.characters;

export const selectAllCharactersIds = createSelector(
  selectCharacterState,
  charactersSelectors.selectIds
);

export const createSelectCharacterById = (id: number | string) => {
  return (state: RootState) =>
    charactersSelectors.selectById(state.characters, id);
};

export const createSelectStatusOfLoadCharacterById = (id: number | string) => {
  return (state: RootState) => state.characters.status[id];
};

export const selectStatusOfLoadCharacters = (state: RootState) =>
  state.characters.status["all"];
export const selectCurrentCharactersQuery = (state: RootState) =>
  state.characters.query;

export const selectCurrentCharactersOffset = (state: RootState) =>
  state.characters.offset;

//=================================================
// Reducer
//=================================================

const charactersReducer = charactersSlice.reducer;
export default charactersReducer;

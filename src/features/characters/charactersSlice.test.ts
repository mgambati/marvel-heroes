import { nanoid } from "@reduxjs/toolkit";
import { setupMockedStore } from "../../common/test-utils";
import * as charactersApi from "./charactersApi";
import charactersReducer, {
  loadCharacters,
  loadSingleCharacter,
} from "./charactersSlice";
import {
  CHARACTERS_RESPONSE_PAYLOAD,
  SINGLE_CHARACTERS_RESPONSE_PAYLOAD,
} from "./__fixtures__/charactersRequests";

const [mockStore, initialState] = setupMockedStore();

describe("charactersSlices", () => {
  describe("reducers and actions", () => {
    const loadCharactersArgs = { query: undefined, offset: 0 };

    test(loadCharacters.pending.type, () => {
      const requestId = nanoid();
      const state = charactersReducer(
        undefined,
        loadCharacters.pending(requestId, loadCharactersArgs)
      );
      expect(state.status["all"]).toBe("pending");
    });

    test(loadCharacters.fulfilled.type, () => {
      const requestId = nanoid();

      const state = charactersReducer(
        undefined,
        loadCharacters.fulfilled(
          CHARACTERS_RESPONSE_PAYLOAD,
          requestId,
          loadCharactersArgs
        )
      );

      expect(state.status["all"]).toBe("idle");
      expect(state.ids).toHaveLength(2);
      expect(state.query).toBe(loadCharactersArgs.query);
    });

    test(loadCharacters.rejected.type, () => {
      const requestId = nanoid();
      const state = charactersReducer(
        undefined,
        loadCharacters.rejected(
          new Error("Mocked Error"),
          requestId,
          loadCharactersArgs
        )
      );
      expect(state.status["all"]).toBe("error");
    });

    test(loadSingleCharacter.pending.type, () => {
      const characterId = "1011334";
      const requestId = nanoid();
      const state = charactersReducer(
        undefined,
        loadSingleCharacter.pending(requestId, characterId)
      );
      expect(state.status[characterId]).toBe("pending");
    });

    test(loadSingleCharacter.fulfilled.type, () => {
      const characterId = "1011334";
      const requestId = nanoid();

      const state = charactersReducer(
        undefined,
        loadSingleCharacter.fulfilled(
          SINGLE_CHARACTERS_RESPONSE_PAYLOAD,
          requestId,
          characterId
        )
      );

      expect(state.status[characterId]).toBe("idle");
      expect(state.ids).toHaveLength(1);
      expect(state.entities[characterId]).toBe(
        SINGLE_CHARACTERS_RESPONSE_PAYLOAD
      );
    });

    test(loadSingleCharacter.rejected.type, () => {
      const characterId = "1011334";
      const requestId = nanoid();
      const state = charactersReducer(
        undefined,
        loadSingleCharacter.rejected(
          new Error("Mocked Error"),
          requestId,
          characterId
        )
      );
      expect(state.status[characterId]).toBe("error");
    });
  });

  describe("async thunks", () => {
    beforeEach(() => {
      jest
        .spyOn(charactersApi, "fetchCharacters")
        .mockResolvedValueOnce(CHARACTERS_RESPONSE_PAYLOAD);

      jest
        .spyOn(charactersApi, "fetchSingleCharacter")
        .mockResolvedValueOnce(SINGLE_CHARACTERS_RESPONSE_PAYLOAD);
    });

    test("loadCharacters", async () => {
      const loadCharactersArgs = { query: undefined, offset: 0 };
      const store = mockStore(initialState);

      const result = await store.dispatch(loadCharacters(loadCharactersArgs));

      const expectedActions = [
        loadCharacters.pending(result.meta.requestId, loadCharactersArgs),
        loadCharacters.fulfilled(
          CHARACTERS_RESPONSE_PAYLOAD,
          result.meta.requestId,
          loadCharactersArgs
        ),
      ];

      expect(charactersApi.fetchCharacters).toHaveBeenCalledTimes(1);
      expect(store.getActions()).toEqual(expectedActions);
    });

    test("loadSingleCharacter", async () => {
      const store = mockStore(initialState);

      const CHARACTER_ID = "1011334";
      const result = await store.dispatch(loadSingleCharacter(CHARACTER_ID));

      const expectedActions = [
        loadSingleCharacter.pending(result.meta.requestId, CHARACTER_ID),
        loadSingleCharacter.fulfilled(
          SINGLE_CHARACTERS_RESPONSE_PAYLOAD,
          result.meta.requestId,
          CHARACTER_ID
        ),
      ];

      expect(charactersApi.fetchSingleCharacter).toHaveBeenCalledTimes(1);
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});

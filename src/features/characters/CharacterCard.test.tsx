import React from "react";
import { render } from "../../common/test-utils";
import CharacterCard from "./CharacterCard";
import * as charactersSlice from "./charactersSlice";
import { SINGLE_CHARACTERS_RESPONSE_PAYLOAD } from "./__fixtures__/charactersRequests";

test("renders correctly", () => {
  jest
    .spyOn(charactersSlice, "createSelectCharacterById")
    .mockReturnValue(() => {
      return SINGLE_CHARACTERS_RESPONSE_PAYLOAD;
    });

  const { container } = render(
    <CharacterCard id={SINGLE_CHARACTERS_RESPONSE_PAYLOAD.id} />
  );

  expect(container).toMatchSnapshot();
});

import React, { FC, useMemo, useEffect } from "react";
import {
  createSelectSeriesByCharacter,
  loadSeriesByCharacter,
  createSelectStatusOfLoadSeriesByCharacter,
} from "./seriesSlice";
import { useSelector, useDispatch } from "react-redux";
import { Grid, Flex, Spinner } from "theme-ui";
import SeriesCard from "./SerieCard";

export interface SeriesListProps {
  characterId: string | number;
}

const SeriesList: FC<SeriesListProps> = ({ characterId }) => {
  const selectSeriesByCharacter = useMemo(
    () => createSelectSeriesByCharacter(characterId),
    [characterId]
  );
  const selectStatusOfLoadSeriesByCharacter = useMemo(
    () => createSelectStatusOfLoadSeriesByCharacter(characterId),
    [characterId]
  );
  const series = useSelector(selectSeriesByCharacter);
  const status = useSelector(selectStatusOfLoadSeriesByCharacter);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(loadSeriesByCharacter(characterId));
  }, [dispatch, characterId]);

  if (status === "pending") {
    return (
      <Flex sx={{ justifyContent: "center" }}>
        <Spinner />
      </Flex>
    );
  }

  return (
    <Grid columns="1fr 1fr 1fr 1fr">
      {series?.map((id) => (
        <SeriesCard key={id} id={id} />
      ))}
    </Grid>
  );
};

export default SeriesList;

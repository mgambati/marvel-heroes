import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
  Dictionary,
} from "@reduxjs/toolkit";
import { RootState } from "../../app/store";
import { ResourceStatus } from "../../common/types";
import { fetchSeriesByCharacter } from "./seriesApi";
import { Series } from "./types";

//=================================================
// Interfaces
//=================================================

export interface SeriesState {
  status: Dictionary<ResourceStatus>;
  byCharacter: Dictionary<number[]>;
}

//=================================================
// Thunks
//=================================================

export const loadSeriesByCharacter = createAsyncThunk(
  "series/loadSeriesByCharacter",
  async (characterId: number | string) => {
    return await fetchSeriesByCharacter(characterId);
  }
);

//=================================================
// Slice
//=================================================

const seriesAdapter = createEntityAdapter<Series>();

const seriesSlice = createSlice({
  name: "characters",
  initialState: seriesAdapter.getInitialState<SeriesState>({
    byCharacter: {},
    status: {},
  }),
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(loadSeriesByCharacter.pending, (state, action) => {
      const characterId = action.meta.arg;
      const key = `character.${characterId}`;
      state.status[key] = "pending";
    });

    builder.addCase(loadSeriesByCharacter.fulfilled, (state, action) => {
      const characterId = action.meta.arg;
      const key = `character.${characterId}`;
      const ids = action.payload.results.map((item) => item.id);

      state.status[key] = "idle";
      state.byCharacter[characterId] = ids;
      seriesAdapter.upsertMany(state, action.payload.results);
    });

    builder.addCase(loadSeriesByCharacter.rejected, (state, action) => {
      const characterId = action.meta.arg;
      const key = `character.${characterId}`;

      state.status[key] = "error";
    });
  },
});

//=================================================
// Selectors
//=================================================

const seriesSelectors = seriesAdapter.getSelectors();

export const createSelectSeriesByCharacter = (characterId: number | string) => {
  return (state: RootState) => state.series.byCharacter[characterId];
};

export const createSelectSeriesById = (id: number | string) => {
  return (state: RootState) => seriesSelectors.selectById(state.series, id);
};

export const createSelectStatusOfLoadSeriesByCharacter = (
  characterId: number | string
) => {
  return (state: RootState) => state.series.status[`character.${characterId}`];
};

//=================================================
// Reducer
//=================================================

const seriesReducer = seriesSlice.reducer;
export default seriesReducer;

import { ImageResource } from "../../common/types";

export interface Series {
  id: number;
  title: string;
  description: string;
  thumbnail: ImageResource;
}

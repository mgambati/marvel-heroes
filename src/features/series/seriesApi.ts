import api from "../../common/api";
import { PaginatedResponse } from "../../common/types";
import { Series } from "./types";

export async function fetchSeriesByCharacter(characterId: number | string) {
  const { data: response } = await api.get<PaginatedResponse<Series>>(
    `/characters/${characterId}/series`
  );

  return response.data;
}

export interface PaginatedResponse<T> {
  code: number;
  status: string;
  data: {
    offset: number;
    limit: number;
    total: number;
    count: number;
    results: T[];
  };
}

export type ResourceStatus = "idle" | "pending" | "error";

export interface FulfilledAction<PromiseResult, ThunkArg = any> {
  type: string;
  payload: PromiseResult;
  meta: {
    requestId: string;
    arg: ThunkArg;
  };
}

export interface ImageResource {
  path: string;
  extension: string;
}
